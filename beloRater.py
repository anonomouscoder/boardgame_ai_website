class BeloCalculator:
    def __init__(self, k=32, pos_range=400):
        self.K = k
        self.range = pos_range

    def calculate_new_ratings(self, elo_scores_in_order_of_current_game_results):
        new_score_list = []
        number_of_players_minus_one = len(elo_scores_in_order_of_current_game_results) - 1
        for i,score in enumerate(elo_scores_in_order_of_current_game_results):
            game_score = 1.0 - (i*(1.0/number_of_players_minus_one))
            total_belo_scores = sum(elo_scores_in_order_of_current_game_results)
            score_diff = (score*len(elo_scores_in_order_of_current_game_results)) - total_belo_scores
            if score_diff > self.range:
                expected_score = 1
            elif score_diff < (self.range*-1):
                expected_score = 0
            else:
                expected_score = float(score)/float(total_belo_scores)
            new_score_list.append(self.get_new_belo(score, game_score, expected_score))
        return new_score_list

    def get_new_belo(self,previous_elo, actual_score, expected_score):
        return int(previous_elo + self.K*(actual_score-expected_score))