class EloCalculation:
    def __init__(self, k=32, spread=400):
        self.K_VALUE = k
        self.ELO_SPREAD = spread

    def calculateExpectedRating(self, scoreRatio, winner, loser):
        """
                      (         score_player          )   (                   1                    )
        @return = K * ( ----------------------------- ) - ( -------------------------------------- )
                      ( score_player + score_opponent )   (          ( elo_loser - elo_winner )    )
                                                          (          ( ---------------------- )    )
                                                          (          (        spread          )    )
                                                          (    1 + 10^                             )
        """
        return int(round(self.K_VALUE * ( scoreRatio - 1/( 1 + 10**(float(loser-winner)/self.ELO_SPREAD)))) )
        
    def normalizeScores(self, listOfScores):
        """
        purpose: divide each number by the number in the first index of the list
        parameters: list of numbers
        returns: new list of numbers, were new_list[i] == normalized(old_list[i]), for all i
        """
        normalizedScores = list(listOfScores)
        normalizedScores[0] = 1
        for i,score in enumerate(listOfScores):
            normalizedScores[i] = float(score) / listOfScores[0]
        return normalizedScores
        
    def updateEloRatings(self, listOfEloRatings, listOfScores):
        """
        purpose: return a modified list of ELO ratings based on placement and score of a single match.
        listOfEloRatings: [elo_1, elo_2, ..., elo_n]
        listOfScores: [score_1, score_2, ..., score_n]
        Both lists are assumed to be sorted, with the first index is assumed to be first place (and highest score)
        """
        listOfScores = self.normalizeScores(listOfScores)
        
        updatedListOfEloRatings = list(listOfEloRatings)
                
        #2-d array of booleans to see if every matchup has been played
        played = [[False for i in listOfEloRatings] for i in listOfEloRatings]
        for i, value in enumerate(played):
            played[i][i] = True
            
        #loop through every player and every opponent combination
        for i, player in enumerate(listOfEloRatings):
            for j, opponent in enumerate(listOfEloRatings):
                if not played[i][j] and not played[j][i]:
                    scoreRatio = float(listOfScores[i]) / float(listOfScores[i]+listOfScores[j])
                    delta = self.calculateExpectedRating(scoreRatio, listOfEloRatings[i], listOfEloRatings[j])
                    updatedListOfEloRatings[i] += delta
                    updatedListOfEloRatings[j] -= delta
                    played[i][j] = True
                    played[j][i] = True
            
        return updatedListOfEloRatings