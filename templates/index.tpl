<html>
    <head>
    <title>Battleline home page</title>
    
    <script type="text/javascript">
    	function runBots() {
    		processBots("runServer");
    	}
    	
    	function seeHeadToHeadResults() {
    		processBots("headToHead");
    	}
    	
    	function getBot(num){
    		var e = document.getElementById("bot"+num);
    		return e.options[e.selectedIndex].value;
    	}
    	
    	function processBots(address) {
    		if (getBot(1) == getBot(2)) {
    			alert("Bots must be different");
    		}
    		else {
    			window.location.href = "/"+address+"/"+getBot(1)+"/"+getBot(2);
    		}
    	}
    </script>
    </head>
    <body>
    
    <h1>General Info</h1>
    <a href="/lastResults">See the latest Results</a>
    <br /> Bot1 <select id="bot1">
         %for bot, player in bots.items():
          <option value="{{bot}}">{{bot}}-{{player["name"]}}</option>
         % end 
         </select>
         Bot2 <select id="bot2">
          %for bot, player in bots.items():
          <option value="{{bot}}">{{bot}}-{{player["name"]}}</option>
         % end 
         </select> <button onclick="runBots()">Run Bots</button> <button onclick="seeHeadToHeadResults()">See Head-To-Head Results</button>
    
    <h1>Win Loss Table</h1>
    <table border="1">
       <tr><th>Bot</th> <th>Player</th> <th>Version</th> 
       <th>North Wins</th> <th>North Losses</th> <th>North %</th> 
       <th>South Wins</th> <th>South Losses</th> <th>South %</th> 
       <th>Total Wins</th> <th>Total Losses</th> <th>Total %</th> 
       <th>elo</th> <th>belo</th></tr>
       % for items in stat_block:
          <tr>
             % for item in items:
               <td>{{item}}</td>
             %end
          </tr>
       % end
       
    </table>
    
     <h1>Bots</h1>
      <table border="1">
       <tr><th>Bot</th> <th>Player</th> <th>Repo</th></tr>
       % for player, info in bots.items():
          <tr>
            <td>{{info["name"]}}</td> 
            <td>{{player}}</td> 
            <td> {{info["repo"]}}</td>
          </tr>
       % end
       
    </table>
   
    
    </body>
</html>
