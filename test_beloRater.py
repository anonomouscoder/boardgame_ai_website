from beloRater import BeloCalculator

def test_equalPreviousScoresTwoPlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000])
    assert returnedElos[0] == 1016
    assert returnedElos[1] == 984

def test_equalPreviousScoresThreePlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000,1000])
    assert returnedElos[0] == 1021
    assert returnedElos[1] == 1005
    assert returnedElos[2] == 989

def test_unequalPreviousScoresThreePlayerUnexpected():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([800, 1200, 1000])
    assert returnedElos[0] == 823
    assert returnedElos[1] == 1203
    assert returnedElos[2] == 989

def test_unequalPreviousScoresThreePlayerUnexpectedOneReallyLow():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([800, 1400, 100])
    assert returnedElos[0] == 820
    assert returnedElos[1] == 1396
    assert returnedElos[2] == 98


def test_equalPreviousScoresFourPlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000,1000,1000])
    assert returnedElos[0] == 1024
    assert returnedElos[1] == 1013
    assert returnedElos[2] == 1002
    assert returnedElos[3] == 992