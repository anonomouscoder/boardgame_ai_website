import eloCalculator

helper = eloCalculator.EloCalculation()
        
#2 player tests
def test_updateEloRatings_2p_even():
    elos = [1000,1000]
    scores = [1,0]
    expectedDiff = 16
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_uneven_expected():
    elos = [1200,800]
    scores = [1,0]
    expectedDiff = 3
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_uneven_unexpected():
    elos = [800,1200]
    scores = [1,0]
    expectedDiff = 29
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_close_game():
    elos = [1000,1000]
    scores = [100,99]
    expectedDiff = 0
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_close_game_lowScore():
    elos = [1000,1000]
    scores = [4,3]
    expectedDiff = 2
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_close_game_lowScore():
    elos = [1000,1000]
    scores = [30,29]
    expectedDiff = 0
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_uneven_close_game():
    elos = [800,1000]
    scores = [100,99]
    expectedDiff = 8
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_blowout():
    elos = [1000,1000]
    scores = [100,1]
    expectedDiff = 16
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
#3 player tests
def test_updateEloRatings_3p_20pointSpread():
    elos = [1000,1000,1000]
    scores = [100,90,80]
    expectedDeltas = [3,0,-3]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]

def test_updateEloRatings_3p_40pointSpread():
    elos = [1000,1000,1000]
    scores = [100,80,60]
    expectedDeltas = [6,0,-6]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_downToTiebreaker():
    elos = [1000,1000,1000]
    scores = [10.1,10,9.9]
    expectedDeltas = [0,0,0]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_heWon_letsCompeteForSecond():
    elos = [1000,1000,1000]
    scores = [100,10.1,10]
    expectedDeltas = [26,-13,-13]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]

def test_updateEloRatings_3p_basicallyPlayerElimination():
    elos = [1000,1000,1000]
    scores = [100.1,100,10]
    expectedDeltas = [13,13,-26]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_uneven_downToTiebreaker():
    elos = [1200,1000,1000]
    scores = [100.1,100,10]
    expectedDeltas = [-3,21,-18]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_uneven_betterWon_letsCompeteForSecond():
    elos = [1200,1000,1000]
    scores = [100,10.1,10]
    expectedDeltas = [10,-5,-5]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_uneven_worseWon_letsCompeteForSecond():
    elos = [800,1000,1000]
    scores = [100,10.1,10]
    expectedDeltas = [42,-21,-21]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]

def test_updateEloRatings_3p_uneven_betterPlayerElimination():
    elos = [1000,1000,1200]
    scores = [100.1,100,10]
    expectedDeltas = [21,21,-42]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_uneven_worsePlayerElimination():
    elos = [1000,1000,800]
    scores = [100.1,100,10]
    expectedDeltas = [5,5,-10]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_super_uneven_expected():
    elos = [1500,800,800]
    scores = [100,1,1]
    expectedDeltas = [0,0,0]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_super_uneven_tie():
    elos = [1500,500,500]
    scores = [100.1,100,99]
    expectedDeltas = [-32,16,16]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
    
def test_updateEloRatings_3p_super_uneven_upset():
    elos = [500,500,1500]
    scores = [100,1,1]
    expectedDeltas = [48,0,-48]
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2]]
        
#4 player tests
def test_updateEloRatings_4p_even():
    elos = [1000,1000,1000,1000]
    scores = [10.4,10.3,10.2,10.1]
    expectedDeltas = [0,0,0,0]    
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2], elos[3]+expectedDeltas[3]]

def test_updateEloRatings_4p_20pointSpread():
    elos = [1000,1000,1000,1000]
    scores = [100,93,86,80]
    expectedDeltas = [4,1,-1,-4]    
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2], elos[3]+expectedDeltas[3]]
    
def test_updateEloRatings_4p_40pointSpread():
    elos = [1000,1000,1000,1000]
    scores = [100,87,73,60]
    expectedDeltas = [7,3,-1,-9]    
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2], elos[3]+expectedDeltas[3]]

def test_updateEloRatings_4p_twoBrackets():
    elos = [1000,1000,1000,1000]
    scores = [100,99,50,49]
    expectedDeltas = [10,10,-10,-10]    
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2], elos[3]+expectedDeltas[3]]

def test_updateEloRatings_4p_winnerOutlier():
    elos = [1000,1000,1000,1000]
    scores = [100,50,40,30]
    expectedDeltas = [21,1,-7,-15]    
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDeltas[0], elos[1]+expectedDeltas[1], elos[2]+expectedDeltas[2], elos[3]+expectedDeltas[3]]

#Varying K_VALUE

def test_updateEloRatings_2p_even_highK():
    helper = eloCalculator.EloCalculation(k=100)
    elos = [1000,1000]
    scores = [1,0]
    expectedDiff = 50
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_oneSided_highK():
    helper = eloCalculator.EloCalculation(k=100)
    elos = [1000,1000]
    scores = [100,0]
    expectedDiff = 50
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_tie_highK():
    helper = eloCalculator.EloCalculation(k=100)
    elos = [1000,1000]
    scores = [100,99]
    expectedDiff = 0
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_even_lowK():
    helper = eloCalculator.EloCalculation(k=10)
    elos = [1000,1000]
    scores = [1,0]
    expectedDiff = 5
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_oneSided_lowK():
    helper = eloCalculator.EloCalculation(k=10)
    elos = [1000,1000]
    scores = [100,0]
    expectedDiff = 5
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_tie_lowK():
    helper = eloCalculator.EloCalculation(k=10)
    elos = [1000,1000]
    scores = [100,99]
    expectedDiff = 0
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]

#Varying SPREAD
def test_updateEloRatings_2p_even_highSpread():
    helper = eloCalculator.EloCalculation(spread=1000)
    elos = [1000,1000]
    scores = [1,0]
    expectedDiff = 16
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_significant_highSpread():
    helper = eloCalculator.EloCalculation(spread=1000)
    elos = [1000,1200]
    scores = [1,0]
    expectedDiff = 20
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_nochance_highSpread():
    helper = eloCalculator.EloCalculation(spread=1000)
    elos = [1000,1400]
    scores = [1,0]
    expectedDiff = 23
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_even_lowSpread():
    helper = eloCalculator.EloCalculation(spread=10)
    elos = [1000,1000]
    scores = [1,0]
    expectedDiff = 16
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_significant_lowSpread():
    helper = eloCalculator.EloCalculation(spread=10)
    elos = [1000,1200]
    scores = [1,0]
    expectedDiff = 32
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]
    
def test_updateEloRatings_2p_nochance_lowSpread():
    helper = eloCalculator.EloCalculation(spread=10)
    elos = [1000,1400]
    scores = [1,0]
    expectedDiff = 32
    assert helper.updateEloRatings(elos,scores) == [elos[0]+expectedDiff, elos[1]-expectedDiff]